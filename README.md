**Contact** info: jens.schwamborn@uni.lu or gemma.gomezgiro@uni.lu

**Please cite:** Synapse alterations precede neuronal damage and storage pathology in a human cerebral organoid model of CLN3-juvenile neuronal ceroid lipofuscinosis (2019) - Acta Neuropathologica Communications (https://doi.org/10.1186/s40478-019-0871-7)

Complete dataset can be found here: https://r3lab.uni.lu/frozen/synapse-alterations-precede-neuronal-damage-and-storage-pathology-in-a-human-cerebral-organoid-model/
